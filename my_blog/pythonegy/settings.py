import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = True

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = [
    'widget_tweaks',
    'tinymce',
    'taggit',
    'haystack',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'app_blog',
    'app_about',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'pythonegy.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'pythonegy.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'EET'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

# Static.
STATIC_URL = '/static/'
# static directory during development.
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'development_static'),
]
# serve static files on the web server.
STATIC_ROOT = os.path.join(BASE_DIR, 'collected_static')

# NOTE can't use mutual folder for STATICFILES_DIRS and STATIC_ROOT, since
# the last one depends on STATICFILES_DIRS to know where the local static lives.


MEDIA_URL = '/media/'
# serve uploaded files from ORM field e.g image field
MEDIA_ROOT = os.path.join(BASE_DIR, 'uploaded_media')

# tinymce

TINYMCE_DEFAULT_CONFIG = {
    'plugins': 'link image preview codesample contextmenu table code lists fullscreen textcolor charmap',
    'toolbar1': 'formatselect insert charmap | forecolor backcolor | bold italic underline strikethrough removeformat | alignleft aligncenter alignright | bullist numlist | codesample code fullscreen',
    # hide the default menu with dropdown items.
    'menubar': False,
    'inline': False,
    'statusbar': True,
    'height': 360,
    'width': 700,
    # arabic direction.
    'directionality': 'rtl',
    # paste the text only, and get rid of other elements.
    'paste_as_text': True,
    # dropdown for programming language code insertion.
    'codesample_languages': [
        {'text': 'Python', 'value': 'python'},
        {'text': 'HTML/XML', 'value': 'markup'},
        {'text': 'CSS', 'value': 'css'},
        {'text': 'JavaScript', 'value': 'javascript'},
        {'text': 'Django', 'value': 'django'}
    ],
    'block_formats': 'Normal=p; Title=h3; Subtitle=h4;',
}

# taggit

TAGGIT_CASE_INSENSITIVE = True

# haystack modifications.

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(os.path.dirname(__file__), 'search_index'),
    },
}

# make Haystack auto indexing new posts.

HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
