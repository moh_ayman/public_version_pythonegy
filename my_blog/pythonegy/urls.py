from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from app_about import views


urlpatterns = [
    # haystack
    path('search/', include('haystack.urls')),
    # tinymce.
    path('tinymce/', include('tinymce.urls')),

    # admin
    path('admin/', admin.site.urls),
    # post list.
    path('', include('app_blog.urls')),
    # about.
    path('about/', views.about, name='about'),
]

# this only suitable fpr devlopmnet
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
