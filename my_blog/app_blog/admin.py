from django.utils.safestring import mark_safe
from django.shortcuts import redirect
from django.contrib import admin

from app_blog.models import Post, PostCategory

from taggit.admin import Tag


class PostAdmin(admin.ModelAdmin):

    def response_delete(self, request, obj_display, obj_id):
        # redirect to the post list page ofter deletion
        return redirect('post_list')

    readonly_fields = ['post_show_image', 'post_slug']

    fields = ['post_go_public', 'post_title', 'post_content', 'post_description', 'post_categories',
              'post_keywords', 'post_image_alt_txt', 'post_image',
              'post_slug', 'post_show_image', 'post_created_on']

    def post_show_image(self, obj):
        """ define new field to show up image in Admin Index"""
        return mark_safe(f"<img src='{obj.post_image.url}' width='50%' height='50%'>")


admin.site.register(Post, PostAdmin)
admin.site.register(PostCategory)
admin.site.unregister(Tag)
