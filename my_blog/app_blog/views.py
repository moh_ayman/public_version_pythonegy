from django.shortcuts import render, redirect, render_to_response

from app_blog.models import Post, PostCategory
from app_blog.none_views import unbroken_url, page_paginate


# home page
def post_list(request):
    posts = Post.objects.all()

    # Page paginator
    paginator = page_paginate(request=request, pages=posts)

    # categories appears on the side widget.
    categories = PostCategory.objects.all()

    # tell the visitor what they read about.
    feedback_msg = 'هنا كل الدروس الموجودة بالموقع, تقدر تستخدم التاج ( الكلمة\
     ال بعد # ) لاظهار دروس معينة، او ابحث من اعلى الصفحة عن عنوان او كلمة \
     وهيظرهرلك المقال الخاص به'

    # page description for SEO
    description = 'Learn Python and web development in Arabic, and other tech terms and fields.'

    # key words for SEO
    keywords = ['django backend', 'django cms', 'cms', 'backend', 'learn',
                'understand', 'programming', 'program', 'arabic', 'egypt',
                'in', 'online', 'website', 'article', 'blog', 'written',
                'tutorial', 'py', 'tut',
                'عربي', 'بايثون', 'جانجو', 'تعلم', 'لغة', 'برمجة', 'استخدام',
                'كورس', 'موقع', 'مقال', 'مكتوب', 'العربية', 'ببساطة', '3',
                'بايثون 3']

    # Context.
    context = {
        'posts': posts,
        'page_obj': paginator,
        'categories': categories,
        'keywords': keywords,
        'description': description,
        'feedback_msg': feedback_msg,
        'url': request.get_full_path(),
        'title': 'Arabic Python Blog | PythonEgy'
    }
    return render(request, 'app_blog/post-list.html', context)


# list posts related to one category.
def post_category(request, category):
    posts = Post.objects.filter(
        post_categories__name__contains=category).order_by('-post_created_on')

    # Page paginator.
    paginator = page_paginate(request=request, pages=posts)

    # categories appears on the side widget.
    categories = PostCategory.objects.all()
    # avoid unknown category ( page with no model objects would appears )
    try:
        feedback_msg = PostCategory.objects.get(name=category).feedback_msg
    except:
        return redirect('post_list')

    context = {
        'page_obj': paginator,
        'categories': categories,
        'url': request.get_full_path(),
        'feedback_msg': feedback_msg,
        'title': f'{category} | PythonEgy'
    }
    return render(request, 'app_blog/post-list.html', context)


def post_details(request, pk, post_slug):
    # unbroken_url(model, *slug_validators, **pk_lookups)
    post, post_details_url = unbroken_url(Post, {'post_slug': post_slug}, {'pk': pk})
    # when user enter wrong slug-field, but correct post ID.
    if post_details_url:
        return redirect(post_details_url)

    # taggit keywords
    post_keywords = post.post_keywords.names()

    # categories appears on the side widget.
    categories = PostCategory.objects.all()

    # Context.
    context = {
        # i get categories from the post object.
        'post': post,
        'post_image': post.post_image.url,
        'description': post.post_description,
        'categories': categories,
        'url': request.get_full_path(),
        'title': f'{post.post_title} | PythonEgy',
        'keywords': post_keywords,
    }
    return render(request, 'app_blog/post-details.html', context)


# redesign the 404 page
def handler404(request, exception, template_name='404.html'):
    response = render_to_response(template_name)
    response.status_code = 404
    return response
