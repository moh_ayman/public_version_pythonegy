from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator


# unbroken_url(model, *slug_validators, **pk_lookups)
def unbroken_url(model_name=None, slug_validators=None, pk_lookups=None):
    """ Re-mapping to the right view whenever its link is
        broken, e.g the user insert space between the URL characters. """

    # get the object
    blog_obj = get_object_or_404(model_name, **pk_lookups)

    # loop over key and values within slug_validators.
    for key, value in slug_validators.items():
        # when slug-field doesn't match the object.
        if value != getattr(blog_obj, key):
            # return the object and its absolute URL.
            return blog_obj, blog_obj.get_absolute_url()
    return blog_obj, None


def page_paginate(request, pages):
    paginator = Paginator(pages, 6)
    page = request.GET.get('page')
    page_obj = paginator.get_page(page)

    return page_obj
