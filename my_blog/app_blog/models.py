from django.utils import text, timezone
from django.db import models
from django.urls import reverse

from taggit.managers import TaggableManager
from tinymce import HTMLField


class PostCategory(models.Model):
    name = models.CharField(max_length=20)
    feedback_msg = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Post(models.Model):
    post_content = HTMLField()
    post_keywords = TaggableManager()
    post_title = models.CharField(max_length=55)
    post_description = models.CharField(max_length=155)
    post_created_on = models.DateTimeField(default=timezone.now)
    post_last_modified = models.DateTimeField(auto_now=True)
    # width (720) with height(350)
    post_image = models.ImageField(upload_to='blog_post_images/', default='pythonegy_blog_post_default_image.png')
    post_image_alt_txt = models.CharField(max_length=123, default='gray background image with no picture logo')
    post_categories = models.ManyToManyField('PostCategory')
    post_slug = models.SlugField(unique=True, blank=True, allow_unicode=True)
    post_go_public = models.BooleanField(default=False)

    class Meta:
        ordering = ['-post_created_on']

    def get_absolute_url(self):
        return reverse('post_details',
                       kwargs={'pk': self.pk, 'post_slug': self.post_slug})

    def __str__(self):
        return self.post_title.title()

    def save(self, *args, **kwargs):
        # when post slug != post title
        if self.post_slug.replace('-', ' ') != self.post_title:
            self.post_slug = text.slugify(self.post_title, allow_unicode=True)
        super().save()
