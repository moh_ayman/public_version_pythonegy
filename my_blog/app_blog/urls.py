from django.urls import path, re_path

from app_blog import views


handler404 = 'app_blog.views.handler404'

# NOTE: \u0621-\u064A => Arabic uicode range.
# NOTE 1: Tha tail are the some chracters that i escaped
post_details_str = '(?P<pk>[0-9]+)/(?P<post_slug>[\u0621-\u064A-a-zA-Z0-9_\\s%\;\!\@\#\$\^\&\*\(\)\{\}\[\]\-]+)/'

urlpatterns = [
    # list post page.
    path('', views.post_list, name='post_list'),
    # category post page.
    path('subject/<category>/', views.post_category, name='post_category'),
    # details post page.
    re_path(post_details_str + '$', views.post_details, name='post_details'),
]
