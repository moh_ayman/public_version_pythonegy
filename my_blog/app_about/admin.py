from django.contrib import admin

from app_about.models import AboutMe


admin.site.register(AboutMe)
