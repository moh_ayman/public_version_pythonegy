from django.shortcuts import render
from app_about.models import AboutMe


def about(request):
    about_me_context = AboutMe.objects.get(id=1)
    context = {
        'about_me_context': about_me_context,
        'title': 'About Moh Ayman'
    }
    return render(request, 'app_about/author-about.html', context)
