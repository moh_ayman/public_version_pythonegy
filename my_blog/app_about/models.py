from django.db import models

from tinymce import HTMLField


class AboutMe(models.Model):
    context = HTMLField()
